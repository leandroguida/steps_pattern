module Steps
  class CalculateLoss < Step
    def perform(input, result)
      result[:loss] = result[:reference] - result[:energy]
      result
    end
  end
end