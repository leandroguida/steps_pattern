module Steps
  class Energy < Step
    def perform(input, result)
      result[:energy] = 100
      result
    end
  end
end