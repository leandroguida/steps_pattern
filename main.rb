require 'date'
require_relative 'step'
require_relative 'task'
require_relative 'result'
require_relative './steps/energy'
require_relative './steps/reference'

task = Task.new
task.add_step Steps::Energy.new
task.add_step Steps::Reference.new

puts "Without the loss calculation step"
result = task.perform(Date.new(2020, 1, 1))
puts result

require_relative './steps/calculate_loss'
puts "With loss calculation step"
task.add_step Steps::CalculateLoss.new
result = task.perform(Date.new(2020, 1, 1))
puts result

