# Steps Pattern

```
ruby main.rb
Without the loss calculation step
{:date=>"2020-01-01", :energy=>100, :reference=>150}
With loss calculation step
{:date=>"2020-01-01", :energy=>100, :reference=>150, :loss=>50}
```