class Task
  def initialize
    @steps = []
  end

  def add_step(task)
    @steps << task
  end

  def perform(input)
    result = Result.new
    result[:date] = input.to_s
    @steps.each_with_object(result) do |step, result|
      result = step.perform(input, result)
    end

    result
  end
end